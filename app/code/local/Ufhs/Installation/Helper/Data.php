<?php

class Ufhs_Installation_Helper_data extends Mage_Core_Helper_Abstract
{
	/**
	 * URL Decode Array
	 * ----------------
	 * Take an array and decode each key and value, then return it.
	 *
	 * @param	$post	Array	The array that needs decoding.
	 * @return	Array	The decoded array.
	 */
	public function url_decode_array($post)
	{
		$return = [];
		foreach($post as $key => $value)
		{
			$return[urldecode($key)] = urldecode($value);
		}
		return $return;
	}

	/**
	 * Upload Handle
	 * -------------
	 * Return a centralised handle to an upload object.
	 *
	 * @param	$inputName	String	The name of the input that needs booting.
	 * @return	Varien_File_Uploader	A handle to the upload object.
	 */
	public function upload_handle($inputName)
	{
		$uploader = new Varien_File_Uploader($inputName);
		$uploader->setAllowedExtensions(array('bmp','gif','jpg','png','jpeg','tiff','svg','pdf','docx','pptx','xlsx','one','pub','doc','xls','ppt','odt','ott','oth','odm','ods','ots','odp','odg','otp','pages','numbers','key','rtf','text','txt','rlc','csv'))
		->setAllowRenameFiles(true)
		->setFilesDispersion(false);
		return $uploader;
	}
}