<?php
/**
 * Installation Data controller
 *
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Adminhtml_InstallationController extends Mage_Adminhtml_Controller_action
{
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('admin/ufhs/installation');
	}
	protected function _initAction()
	{
		$this->_title($this->__('View Data'));
		$this->loadLayout();
		$this->_initLayoutMessages('adminhtml/session');
		$this->_setActiveMenu('ufhs');
		return $this;
	}

	public function indexAction()
	{
		$this->_initAction();
		$this->renderLayout();
	}

	public function gridAction()
	{
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('ufhs_installation/adminhtml_installation_grid')->toHtml()
			);
	}

	public function exportCsvAction()
	{
		$fileName = 'installation-services.csv';
		$content = $this->getLayout()->createBlock('installation/adminhtml_installation_grid')
		->getCsv();
		$this->_sendUploadResponse($fileName, $content);
	}

	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
	{
		$response = $this->getResponse();
		$response->setHeader('HTTP/1.1 200 OK','');
		$response->setHeader('Pragma', 'public', true);
		$response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
		$response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
		$response->setHeader('Last-Modified', date('r'));
		$response->setHeader('Accept-Ranges', 'bytes');
		$response->setHeader('Content-Length', strlen($content));
		$response->setHeader('Content-type', $contentType);
		$response->setBody($content);
		$response->sendResponse();
		die;
	}

	/**
	 * Note Action
	 * -----------
	 * Save a note to a particular customer and a file(s) to that particular note.
	 */
	public function noteAction()
	{
		$post = $this->getRequest()->getParams();
		if(strlen($post['comment']) > 0 && $post['id'] > 0)
		{
			$note = Mage::getModel('installation/note');
			$note->setFK($post['id'])
			->setContent($post['comment'])
			->save();
			$file = $_FILES['attachment'];
			if($file['error'] == 0)
			{
				$_FILES['attachment']['name'] = strtolower(str_replace([" ","&","?"],"-",$_FILES['attachment']['name']));
				$media_path = Mage::getBaseDir('media') . '/ufhs/installation/attachment/' . $note->getTempid();
				$uploader = Mage::helper('installation')->upload_handle('attachment');

				try
				{
					if($uploader->save($media_path, $_FILES['attachment']['name']))
					{
						Mage::getModel('installation/attachment')->setFK($note->getTempid())
						->setContent($_FILES['attachment']['name'])
						->save();
					}
					else
					{
						throw new Exception('Unable to save to that location.');
					}
				}
				catch (Exception $e)
				{
					Mage::getSingleton('core/session')->addError($e->getMessage());
				}
			}
			else {
				if(strlen($file['type']) > 0)
				{
					Mage::getSingleton('core/session')->addError('Failed to upload that file, please try again.');
				}
			}
		}
		else
		{
			Mage::getSingleton('core/session')->addError('Please enter a valid comment.');
		}
		$this->_returnToView();
	}

	/**
	 * Accounts Action
	 * ---------------
	 * Update the total cost and total quote value of a customer, striping out any
	 * invalid chars and ading a ntoe agaisnt that customer.
	 */
	public function accountsAction()
	{
		$id = $this->getRequest()->getParam('id');
		$post = Mage::app()->getRequest()->getParams();
		$cost = preg_replace("/[^0-9,.]/", "", $post['total_cost']);
		$quote = preg_replace("/[^0-9,.]/", "", $post['total_quoted']);
		$accNum = $post['acc_number'];
		$job = Mage::getModel('installation/customer')->load($id);

		$updateNote = 'Accounts updated - ';
		$updateNote .= $job->getTotalCost() === $cost ? '' : 'New Cost: £' . $cost . ', ';
		$updateNote .= $job->getTotalQuoted() === $quote ? '' : 'New Quote: £' . $quote . ', ';
		$updateNote .= $job->getAccNumber() === $accNum ? '' : 'Account Number: ' . $accNum . ', ';
		$updateNote = substr($updateNote, 0, -2);

		if ($updateNote != 'Accounts updated ') {
			$this->_addNote($id, $updateNote);
		}

		$job->addData(
			array(
				'total_quoted' => $quote,
				'total_cost' => $cost,
				'acc_number' => $accNum
				))->setId($id)->save();

		$this->_returnToView();
	}

	/**
	 * View Action
	 * -----------
	 * The function that renders the view for a particular customer.
	 */
	public function viewAction()
	{
		$id = $this->getRequest()->getParam('id');
		$customer = Mage::getModel('installation/customer')->load($id);
		$this->_initAction();
		$this->_title($this->__('Quote #'.$customer->getId()));

		if ($customer->getId() && $id)
		{
			Mage::register('installation-customer-details', $customer);
			$this->_setActiveMenu('ufhs');
			$this->renderLayout();
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('installation')->__('Item does not exist'));
			$this->_returnToGrid();
		}
	}

	private function _returnToView()
	{
		$id = $this->getRequest()->getParam('id');
		$this->_redirect('*/*/view/id/' . $id);
	}
	private function _returnToGrid()
	{
		$this->_redirect('*/*');
	}

	/**
	 * Set Status
	 * ----------
	 * Function for changing a customer's status. Sets the status and creates a note.
	 *
	 * @param	$statusId	Int	The status ID.
	 * @return	Ufhs_Installation_Model_Customer
	 */
	private function _setStatus($statusId)
	{
		$id = $this->getRequest()->getParam('id');
		$statusText = Mage::getModel('installation/status')->load($statusId)->getStatusText();
		$this->_addNote($id, 'Status Update: ' . $statusText);
		return Mage::getModel('installation/customer')->load($id)->addData(array(
			'status_id' => $statusId,
			'state_change' => date('Y-m-d H:i:s')
			))->setId($id)->save();
	}

	public function performstatechangeAction()
	{
		$jobId = $this->getRequest()->getParam('id');
		$state = $this->getRequest()->getParam('state');
		$job = Mage::getModel('installation/customer')->load($jobId);
		$statusText = Mage::getModel('installation/status')->load($state)->getStatusText();
		$customerName = $job->getFirstName() . ' ' . $job->getLastName();

		$emailConfig = [
		0 => [
		'message' => 'A new installation request has been posted on the website.',
		'to' => 'install_manager'
		],
		1 => [
		'message' => 'A survey is required for Job #' . $jobId . '.',
		'to' => $jobId,
		'survey' => TRUE
		],
		2 => [
		'message' => 'The survey has been completed for Job #' . $jobId . ', a customer quote is now required.',
		'to' => 'install_manager'
		],
		// 3 => [
		// 'message' => $customerName . ' has been sent their quote for Job #' . $jobId . '.',
		// 'to' => 'install_manager'
		// ],
		// 4 => [
		// 'message' => 'Installation Job #'. $jobId .' has been approved by the customer. We now need a deposit to be collected from them.',
		// 'to' => 'accounts'
		// ],
		// 5 => [
		// 'message' => 'The deposit for Job #'. $jobId .' has been received so it\'s now time to book the installation.',
		// 'to' => 'install_manager'
		// ],
		6 => [
		'message' => 'Job #'. $jobId .' is ready to be installed.',
		'to' => $jobId,
		'survey' => TRUE
		],
		7 => [
		'message' => 'You have a Job that has been completed, it might be nice to collect some feedback.',
		'to' => 'install_manager'
		],
		8 => [
		'message' => 'Job #'. $jobId .' is now complete.',
		'to' => 'install_manager',
		'returnToGrid' => TRUE
		],
		11 => [
		'message' => 'Job #'. $jobId .' is now complete and ready for billing.',
		'to' => 'accounts'
		]

		];

		$subject = 'Installation Job Update - ' . $statusText . ' (#' . $job->getId() . ')';
		$email = Mage::getModel('installation/email');
		if (isset($emailConfig[$state])) {
			$message = $email->getEmailBody(
				$jobId,
				$statusText,
				$customerName,
				$emailConfig[$state]['message']
				);
			$toAddr = $email->getAddr($emailConfig[$state]['to'], isset($emailConfig[$state]['survey']));
			$email->send($toAddr, $message, $subject);
		}

		$this->_setStatus($state);

		$returnFunction = isset($emailConfig[$state]['returnToGrid']) ? '_returnToGrid' : '_returnToView';
		$this->$returnFunction();
	}

	public function cancelledAction()
	{
		$this->_setStatus($this->_getStatusId('cancelled'));
		$this->_returnToGrid();
	}

	public function requestcancelAction()
	{
		$this->_initAction();
		$this->_title($this->__('Cancellation Request'));
		$this->renderLayout();
	}

	public function submitcancelAction()
	{
		$id = $this->getRequest()->getParam('id');
		$postMessage = $this->getRequest()->getParam('reason');
		$user = $this->getRequest()->getParam('username');
		$job = Mage::getModel('installation/customer')->load($id);
		$customerName = $job->getFirstName() . ' ' . $job->getLastName();

		$this->_addNote($id, 'Cancellation Requested: ' . strip_tags($postMessage));

		$email = Mage::getModel('installation/email');


		$message = $email->getEmailBody(
			$id,
			'Cancellation Request',
			$customerName,
			'The cancellation of Job #' . $id . ' was requested by ' . $user . ' on ' . date('d/m/Y') . ' with the following comment "' . $postMessage . '".'
			);

		$toAddr = $email->getAddr('install_manager');
		$email->send($toAddr, $message, 'Installation Job Update - Cancellation Request (#' . $job->getId() . ')');

		$this->_returnToView();
	}

	public function reportAction()
	{
		$this->_initAction();
		$this->_title($this->__('Overview Report'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function addnewAction()
	{
		$this->_initAction();
		$this->_title($this->__('Add New Job'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function editcustomerAction()
	{
		$this->_initAction();
		$this->_title($this->__('Edit Customer'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function editinstallerAction()
	{
		$this->_initAction();
		$this->_title($this->__('Edit Installer'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function editadvisorAction()
	{
		$this->_initAction();
		$this->_title($this->__('Edit Advisor'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function viewinstallersAction()
	{
		$this->_initAction();
		$this->_title($this->__('Installers'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function viewadvisorsAction()
	{
		$this->_initAction();
		$this->_title($this->__('Advisors'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function installereditAction()
	{
		$this->_initAction();
		$this->_title($this->__('Edit Installer'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function advisoreditAction()
	{
		$this->_initAction();
		$this->_title($this->__('Edit Advisor'));
		$this->_setActiveMenu('ufhs');
		$this->renderLayout();
	}

	public function submitjobAction()
	{
		$postModel = Mage::getModel('installation/post');
		$postData = Mage::app()->getRequest()->getParams();
		$email = Mage::getModel('installation/email');
		// If we have post data
		if(!empty($postData)) {
			// Validate it and check for errors
			$postData = Mage::helper('installation')->url_decode_array($postData);
			// If we don't have errors
			if(!empty($postData['first_name']) && !empty($postData['last_name']) && !empty($postData['email'])) {
				// Try to save the customer, rooms and files
				$customer = Mage::getModel('installation/customer');
				$postData['state_change'] = $postData['created_date'] = date('Y-m-d H:i:s');
				$customer->setData($postData);

				if($customer->save() && $postModel->saveFiles($_FILES, $customer->getId())) {
					$message = $email->getEmailBody(
						$customer->getId(),
						'Awaiting Confirmation',
						$customer->getFirstName() . ' ' . $customer->getLastName(),
						'A new installatioon request has come in.'
						);
					$toAddr = $email->getAddr('install_manager');
					$email->send($toAddr, $message, 'Installation Job Update - Awaiting Confirmation (#' . $customer->getId() . ')');
					$this->_redirect('*/*/view/id/' . $customer->getId());
					return true;
				} else {
					Mage::getSingleton('core/session')->addError("There was a problem saving an installation service record to the database.");
				}

				$id = $customer->getId() ?: 0;
				$message = $email->getEmailBody(
					$id,
					'An error has occured',
					$customer->getFirstName() . ' ' . $customer->getLastName(),
					'An error has occured saving an installation job in the backend. ' . http_build_query($postData)
					);
				$toAddr = $email->getAddr('dev');
				$email->send($toAddr, $message, 'An error has occured with the Installation Service');
			} else {
				Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
			}
		} else {
			Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
		}
		$this->_redirect('*/*/addnew');
		return false;
	}

	public function updatecustomerAction()
	{
		$id = $this->getRequest()->getParam('id');
		$postModel = Mage::getModel('installation/post');
		$postData = Mage::app()->getRequest()->getParams();
		$customer = Mage::getModel('installation/customer')->load($id);

		if (!empty($postData)) {
			$postData = Mage::helper('installation')->url_decode_array($postData);
			$postModel->validatePost($postData);
			if (!$postModel->hasErrors()) {
				$customer->load($id)->setData($postData)->save();
				$this->_addNote($id, 'The customer details have been updated');
			} else {
				Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
			}
		} else {
			Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
		}

		$this->_returnToView();
	}

	public function updateinstalldateAction()
	{
		$id = $this->getRequest()->getParam('id');
		$installdate = $this->getRequest()->getParam('installdate');
		$customer = Mage::getModel('installation/customer')->load($id);
		if (strtotime($installdate)) {
			$customer->setData('install_date', $installdate)->save();
			$this->_addNote($id, 'The installation date has been updated to ' . $installdate);
		} else {
			Mage::getSingleton('core/session')->addError("Please enter a valid date.");
		}
		$this->_returnToView();
	}

	public function updateinstallerAction()
	{
		$id = $this->getRequest()->getParam('id');
		$postData = Mage::app()->getRequest()->getParams();
		$customer = Mage::getModel('installation/customer')->load($id);

		if (!empty($postData)) {
			if (isset($postData['installer_id']) && !is_nan($postData['installer_id'])) {
				$from = Mage::getModel('installation/installers')->load($customer->getInstallerId())->getName();
				$to = Mage::getModel('installation/installers')->load($postData['installer_id'])->getName();

				$state = $customer->getStatusId();
				$customer->load($id)->setData($postData)->save();
				$this->_addNote($id, 'The installer has been updated from ' . $from . ' to ' . $to);

				if ($state == 1) {
					$this->_setStatus($this->_getStatusId('awaiting_confirmation'));
				}
			} else {
				Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
			}
		} else {
			Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
		}

		$this->_returnToView();
	}

	public function updateadvisorAction()
	{
		$id = $this->getRequest()->getParam('id');
		$postData = Mage::app()->getRequest()->getParams();
		$customer = Mage::getModel('installation/customer')->load($id);

		if (!empty($postData)) {
			if (isset($postData['advisor_id']) && !is_nan($postData['advisor_id'])) {
				$from = Mage::getModel('installation/advisors')->load($customer->getAdvisorId())->getName();
				$to = Mage::getModel('installation/advisors')->load($postData['advisor_id'])->getName();

				$customer->load($id)->setData($postData)->save();
				$this->_addNote($id, 'The advisor has been updated from ' . $from . ' to ' . $to);

			} else {
				Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
			}
		} else {
			Mage::getSingleton('core/session')->addError("There was a problem with the data you've provided. Please try again.");
		}

		$this->_returnToView();
	}

	private function _addNote($id, $content)
	{
		Mage::getModel('installation/note')->setFK($id)
		->setContent($content)
		->save();
	}

	public function deletejobAction()
	{
		$currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];
		if ($roleName == 'Administrators') {
			// Setup
			$id = $this->getRequest()->getParam('id');
			$collection = Mage::getModel('installation/additions')
			->getCollection()
			->addFieldToFilter('fkid', $id)
			->addFieldToFilter('type', ['neq' => 'attachment']);
			$assocFiles = [];
			// Compile drawings
			$drawingsDir = Mage::getBaseDir('media') . '/ufhs/installation/drawing/' . $id . '/';
			if (file_exists($drawingsDir)) {
				foreach (scandir($drawingsDir) as $filename) {
					if ($filename !== '.' && $filename !== '..') {
						$assocFiles[] = $drawingsDir . $filename;
					}
				}
			}
			// Compile note attachments and delete them from the db
			foreach ($collection as $item) {
				$additionCollection = Mage::getModel('installation/additions')
				->getCollection()
				->addFieldToFilter('type', 'attachment')
				->addFieldToFilter('fkid', $item->getId());
				$attachmentsDir = Mage::getBaseDir('media') . '/ufhs/installation/attachment/' . $item->getId() . '/';
				foreach($additionCollection as $item) {
					$assocFiles [] = $attachmentsDir . $item->getContent();
					$item->delete();
				}
			}
			// Delete compiled files
			foreach ($assocFiles as $file) {
				if (file_exists($file)) {
					unlink($file);
				}
			}
			// Delete drawings and notes
			foreach ($collection as $item) {
				$item->delete();
			}
			// Delete main job entry
			Mage::getModel('installation/customer')->load($id)->delete();
		} else {
			$this->_setStatus($this->_getStatusId('deleted'));
		}
		$this->_returnToGrid();
	}

	public function installerupdateAction()
	{
		$errors = [];
		$additionalFields = ['address2','telephone','mobile'];
		$postData = $this->getRequest()->getParams();
		foreach ($postData as $key => $value) {
			if (!in_array($key, $additionalFields) && strlen($value) <= 0) {
				$errors[] = 'Missing value for ' . $key;
			}
		}
		if (empty($postData['mobile']) && empty($postData['telephone'])) {
			$errors[] = 'Please provide at least one contact number';
		}
		if (empty($errors)) {
			$installer = Mage::getModel('installation/installers');
			if (isset($postData['id'])) {
				$installer->load($postData['id']);
			}
			$installer->setData($postData)->save();
			$this->_redirect('*/*/viewinstallers');
		} else {
			foreach ($errors as $error) {
				Mage::getSingleton('core/session')->addError($error);
			}
			$url = isset($postData['id']) ? '*/*/installeredit/id/' . $postData['id'] : '*/*/installeredit';
			$this->_redirect($url);
		}
	}

	public function advisorupdateAction()
	{
		$errors = [];
		$postData = $this->getRequest()->getParams();
		foreach ($postData as $key => $value) {
			if (strlen($value) <= 0) {
				$errors[] = 'Missing value for ' . $key;
			}
		}
		if (empty($errors)) {
			$advisor = Mage::getModel('installation/advisors');
			if (isset($postData['id'])) {
				$advisor->load($postData['id']);
			}
			$advisor->setData($postData)->save();
			$this->_redirect('*/*/viewadvisors');
		} else {
			foreach ($errors as $error) {
				Mage::getSingleton('core/session')->addError($error);
			}
			$url = isset($postData['id']) ? '*/*/advisoredit/id/' . $postData['id'] : '*/*/advisoredit';
			$this->_redirect($url);
		}
	}

	private function _getStatusId($code)
	{
		return Mage::getModel('installation/status')->load($code, 'status_code')->getId();
	}
}