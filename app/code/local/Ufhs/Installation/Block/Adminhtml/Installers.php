<?php
/**
 * Installation Data Block
 *
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installers extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_installers';
        $this->_blockGroup = 'installation';
        $this->_headerText = Mage::helper('installation')->__('Installers');
        parent::__construct();
        $this->_removeButton('add');
        $this->addButton('new_add', [
            'label' => 'Add New Installer',
            'onclick' => "setLocation('" . $this->getUrl('*/*/installeredit') . "')",
            'class' => 'add'
            ]);
    }
}