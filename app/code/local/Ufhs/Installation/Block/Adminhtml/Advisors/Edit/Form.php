<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Advisors_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $advisor = Mage::getModel('installation/advisors')->load($id);
        $form = new Varien_Data_Form(array(
            'id' => 'advisoredit',
            'action' => $this->getUrl('*/*/advisorupdate', ['id' => $id]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('editcustomer_form', ['legend' => Mage::helper('installation')->__('Details')]);

        $fieldset->addField('name', 'text', [
            'label' => Mage::helper('installation')->__('Name'),
            'name' => 'name',
            'value' => $advisor->getName() ?: '',
            'required' => true
            ]);
        $fieldset->addField('email', 'text', [
            'label' => Mage::helper('installation')->__('Email'),
            'name' => 'email',
            'value' => $advisor->getEmail() ?: '',
            'required' => true
            ]);
        $fieldset->addField('telephone', 'text', [
            'label' => Mage::helper('installation')->__('Telephone'),
            'name' => 'telephone',
            'value' => $advisor->getTelephone() ?: '',
            'required' => true
            ]);

        return parent::_prepareForm();
    }
}