<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Notes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$id = $this->getRequest()->getParam('id');
		$this->_controller = 'adminhtml_installation_notes';
		$this->_blockGroup = 'installation';
		$this->_headerText = Mage::helper('installation')->__('Notes');

        parent::__construct();
        $this->_removeButton('add');
    }
}