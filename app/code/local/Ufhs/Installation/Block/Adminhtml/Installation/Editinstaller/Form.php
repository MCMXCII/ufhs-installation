<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Editinstaller_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $customer = Mage::getModel('installation/customer')->load($id);
        $form = new Varien_Data_Form(array(
            'id' => 'editinstaller',
            'action' => $this->getUrl('*/*/updateinstaller', ['id' => $id]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('editinstaller_form', array('legend'=>Mage::helper('installation')->__('Installer')));

        $fieldset->addField('installer_id', 'select', [
            'label' => Mage::helper('installation')->__('Installer'),
            'name' => 'installer_id',
            'value' => $customer->getInstallerId(),
            'values' => $this->_getInstallers(),
            'required' => true
            ]);

        return parent::_prepareForm();
    }

    private function _getInstallers()
    {
        $return = [];
        $collection = Mage::getModel('installation/installers')->getCollection();
        foreach ($collection->getData() as $installer) {
            $return[$installer['id']] = $installer['name'];
        }
        return $return;
    }
}