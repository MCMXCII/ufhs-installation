<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Newjob_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'newjob',
            'action' => $this->getUrl('*/*/submitjob'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('newjob_form', array('legend'=>Mage::helper('installation')->__('Information')));

        $fieldset->addField('first_name', 'text', [
            'label' => Mage::helper('installation')->__('First Name'),
            'name' => 'first_name',
            'required' => true
            ]);

        $fieldset->addField('last_name', 'text', [
            'label' => Mage::helper('installation')->__('Last Name'),
            'name' => 'last_name',
            'required' => true
            ]);

        $fieldset->addField('email', 'text', [
            'label' => Mage::helper('installation')->__('Email'),
            'name' => 'email',
            'required' => true
            ]);

        $fieldset->addField('telephone', 'text', [
            'label' => Mage::helper('installation')->__('Telephone'),
            'name' => 'telephone',
            'required' => false
            ]);

        $fieldset->addField('address1', 'text', [
            'label' => Mage::helper('installation')->__('Address Line 1'),
            'name' => 'address1',
            'required' => false
            ]);

        $fieldset->addField('address2', 'text', [
            'label' => Mage::helper('installation')->__('Address Line 2'),
            'name' => 'address2',
            'required' => false
            ]);

        $fieldset->addField('town', 'text', [
            'label' => Mage::helper('installation')->__('Town'),
            'name' => 'town',
            'required' => false
            ]);

        $fieldset->addField('postcode', 'text', [
            'label' => Mage::helper('installation')->__('Postcode'),
            'name' => 'postcode',
            'required' => false
            ]);

        $fieldset->addField('property', 'radios', [
            'label' => Mage::helper('installation')->__('Property Type'),
            'name' => 'property',
            'required' => false,
            'value' => '',
            'values' => [
                ['value' => 'Flat', 'label' => 'Flat'],
                ['value' => 'House', 'label' => 'House'],
                ['value' => 'Commercial', 'label' => 'Commercial']
            ]
            ]);

        $fieldset->addField('project_type', 'radios', [
            'label' => Mage::helper('installation')->__('Project Type'),
            'name' => 'project_type',
            'required' => false,
            'value' => '',
            'values' => [
                ['value' => 'retro', 'label' => 'Retro-Fit'],
                ['value' => 'newbuild', 'label' => 'New Build']
            ]
            ]);

        $fieldset->addField('additional_info', 'textarea', [
            'label' => Mage::helper('installation')->__('Additional Info'),
            'name' => 'additional_info',
            'required' => false
            ]);

        $fieldset->addField('attachment','file',array(
            'label' => Mage::helper('installation')->__('Attach a file'),
            'name' => 'file1',
            'required' => false
            ));

        return parent::_prepareForm();
    }
}