<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Turnaround extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$totalDifference = 0;
		$value =  $row->getData($this->getColumn()->getIndex());
		$collection = Mage::getModel('installation/customer')->getCollection()
		->addFieldToFilter('state_change',array('gteq' => Mage::registry('installation-report-datefrom')))
		->addFieldToFilter('state_change',array('lteq' => Mage::registry('installation-report-dateto')))
		->addFieldToFilter('status_id',$value);
		foreach($collection as $item)
		{
			$created = new DateTime($item->getCreatedDate());
			$changed = new DateTime($item->getStateChange());
			$difference = $changed->getTimestamp() - $created->getTimestamp();
			$totalDifference += $difference;
		}
		$averageDifference = $totalDifference != 0 ? $totalDifference / count($collection) : 0;
		$now = new Datetime(date("Y-m-d H:i:s"));
		$future = $now->getTimestamp() + $averageDifference;
		$dateObj = new Datetime();
		$dateObj->setTimestamp($future);
		return date_diff($dateObj,$now)->format('%a days');
	}
}
?>
