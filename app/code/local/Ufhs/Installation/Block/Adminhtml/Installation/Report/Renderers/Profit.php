<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Profit extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		$collection = Mage::getModel('installation/customer')->getCollection()
		->addFieldToFilter('state_change',array('gteq' => Mage::registry('installation-report-datefrom')))
		->addFieldToFilter('state_change',array('lteq' => Mage::registry('installation-report-dateto')))
		->addFieldToFilter('status_id',$value);
		$quoted = $collection->getColumnValues('total_quoted');
		$cost = $collection->getColumnValues('total_cost');
		$profit = array_sum($quoted) - array_sum($cost);
		return $profit < 0 ? '<span style="color:#f00;">-£' . str_replace("-","",number_format($profit,2)) . '</span>' : '£' . number_format($profit,2);
	}
}
?>