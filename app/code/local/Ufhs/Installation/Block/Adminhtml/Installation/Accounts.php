<?php

/**
* Installation Data Notes
*
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
*/
class Ufhs_Installation_Block_Adminhtml_Installation_Accounts extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function __construct()
    {
        $currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];

        if($roleName != 'Administrators' && $roleName != 'Install Manager' && $roleName != 'Accounts') {
            return false;
        }
        parent::__construct();
        $id = $this->getRequest()->getParam('id');
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation';
        $this->_controller = 'adminhtml_installation';
        $this->_mode = 'accounts';

        $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('back');
        $this->_removeButton('reset');
    }

    public function getHeaderText()
    {
        return Mage::helper('installation')->__('Accounts');
    }
}