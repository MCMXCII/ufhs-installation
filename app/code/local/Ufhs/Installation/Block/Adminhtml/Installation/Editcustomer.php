<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Editcustomer extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
        $id = $this->getRequest()->getParam('id');
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation';
        $this->_controller = 'adminhtml_installation';
        $this->_mode = 'editcustomer';

        $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('back');
        $this->_removeButton('reset');

        $this->addButton('new_back', [
            'label' => 'Back',
            'onclick' => "setLocation('" . $this->getUrl('*/*/view/id/' . $id) . "')",
            'class' => 'back'
            ]);

        $this->addButton('new_save', [
            'label' => 'Update',
            'onclick' => "document.getElementById('editcustomer').submit()",
            'class' => 'add'
            ]);
    }

    public function getHeaderText()
    {
        return Mage::helper('installation')->__('Edit Customer');
    }
}