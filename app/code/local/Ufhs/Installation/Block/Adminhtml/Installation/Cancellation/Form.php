<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Cancellation_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'cancellation',
            'action' => $this->getUrl('*/*/submitcancel', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('newnote_form', array('legend'=>Mage::helper('installation')->__('Cancellation Request')));

        $fieldset->addField('user', 'text', array(
            'label' => Mage::helper('installation')->__('User'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'user',
            'disabled' => 'disabled',
            'value' => ucwords(Mage::getModel('admin/session')->getUser()->getUsername())
            ));
        $fieldset->addField('username', 'hidden', array(
            'class' => 'required-entry',
            'required' => true,
            'name' => 'username',
            'value' => ucwords(Mage::getModel('admin/session')->getUser()->getUsername())
            ));

        $fieldset->addField('datetime', 'text', array(
            'label' => Mage::helper('installation')->__('Date / Time'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'datetime',
            'disabled' => 'disabled',
            'value' => date('d/m/Y H:i:s')
            ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('reason', 'editor', array(
            'label' => Mage::helper('installation')->__('Reason For Cancellation'),
            'name' => 'reason',
            'class' => 'required-entry',
            'required' => true,
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig($wysiwygConfig)
            ));


        return parent::_prepareForm();
    }
}