<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('installationReportGrid');
		$this->setDefaultSort('name');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setPagerVisibility(false);
		$this->setFilterVisibility(false);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('installation/status')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{

		$this->addColumn('status', array(
			'header' => Mage::helper('installation')->__('Status'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'status_text'
			));
		$this->addColumn('quantity', array(
			'header' => Mage::helper('installation')->__('Quantity'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Quantity'
			));
		$this->addColumn('quoted', array(
			'header' => Mage::helper('installation')->__('Total Quoted'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Quoted'
			));
		$this->addColumn('cost', array(
			'header' => Mage::helper('installation')->__('Total Cost'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Cost'
			));
		$this->addColumn('profit', array(
			'header' => Mage::helper('installation')->__('Total Profit'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Profit'
			));
		$this->addColumn('turnaround', array(
			'header' => Mage::helper('installation')->__('Status Reached (Avg)'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Turnaround'
			));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("installation_block_adminhtml_installation_rooms_grid_preparecolumns", array("data" => $object));

		return parent::_prepareColumns();
	}
}