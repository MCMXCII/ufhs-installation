<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Notes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('installationNotesGrid');
		$this->setDefaultSort('when');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$customer = Mage::registry('installation-customer-details');

		$collection = Mage::getModel('installation/additions')->getCollection()
		->addFieldToFilter('type','note')
		->addFieldToFilter('fkid',$customer->getId());
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('who', array(
			'header' => Mage::helper('installation')->__('Who'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'userstamp'
			));
		$this->addColumn('when', array(
			'header' => Mage::helper('installation')->__('When'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'timestamp',
			'type' => 'datetime'
			));
		$this->addColumn('what', array(
			'header' => Mage::helper('installation')->__('Note'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'content'
			));
		$this->addColumn('files', array(
			'header' => Mage::helper('installation')->__('Attachments'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Notes_Renderer'
			));


		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("installation_block_adminhtml_installation_notes_grid_preparecolumns", array("data" => $object));

		return parent::_prepareColumns();
	}
}