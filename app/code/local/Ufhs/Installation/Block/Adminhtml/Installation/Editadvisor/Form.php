<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Editadvisor_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $customer = Mage::getModel('installation/customer')->load($id);
        $form = new Varien_Data_Form(array(
            'id' => 'editadvisor',
            'action' => $this->getUrl('*/*/updateadvisor', ['id' => $id]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('editadvisor_form', array('legend'=>Mage::helper('installation')->__('Advisor')));

        $fieldset->addField('advisor_id', 'select', [
            'label' => Mage::helper('installation')->__('Advisor'),
            'name' => 'advisor_id',
            'value' => $customer->getAdvisorId(),
            'values' => $this->_getAdvisors(),
            'required' => true
            ]);

        return parent::_prepareForm();
    }

    private function _getAdvisors()
    {
        $return = [];
        $collection = Mage::getModel('installation/advisors')->getCollection();
        foreach ($collection->getData() as $advisor) {
            $return[$advisor['id']] = $advisor['name'];
        }
        return $return;
    }
}