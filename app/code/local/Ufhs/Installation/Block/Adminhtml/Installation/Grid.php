<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('installationGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('installation/customer')->getCollection()
		->join(
			Mage::getSingleton('core/resource')->getTableName('status'),
			'`status_id`=status.id',
			['statustext' => 'status_text']
			)
		->addFilterToMap('statustext','status.status_text');

		// Role related filtering on the grid
		$currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];

		if ($roleName != 'Administrators') {
			$collection->addFilterToMap('statuscode','status.status_code')
			->addFieldToFilter('statuscode', ['neq' => 'deleted']);
		}

		if ($roleName == 'Installer') {
			$installerId = Mage::getModel('installation/installers')->load($currentUser, 'user_id')->getId();
			$collection->addFieldToFilter('installer_id', $installerId);
		}

		if ($roleName == 'Accounts') {
			$collection->addFilterToMap('statuscode','status.status_code')
			->addFieldToFilter('statuscode', ['eq' => 'with_accounts']);
		}

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('installation')->__('#'),
			'align' => 'left',
			'width' => '10px',
			'index' => 'id',
			'filter_index'=>'main_table.id'
			));
		$this->addColumn('status', array(
			'header' => Mage::helper('installation')->__('Status'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'statustext',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => true,
			'options' => $this->_getStatuses()
			));
		$this->addColumn('name', array(
			'header' => Mage::helper('installation')->__('Name'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'first_name'
			));
		$this->addColumn('last-name', array(
			'header' => Mage::helper('installation')->__('Last Name'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'last_name'
			));
		$this->addColumn('email', array(
			'header' => Mage::helper('installation')->__('Email'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'email'
			));
		$this->addColumn('postcode', array(
			'header' => Mage::helper('installation')->__('Postcode'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'postcode',
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Postcode_Renderer'
			));
		$this->addColumn('advisor', array(
			'header' => Mage::helper('installation')->__('Sales Advisor'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'advisor_id',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => true,
			'options' => $this->_getAdvisors(),
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Advisors_Renderer'
			));
		$this->addColumn('installer', array(
			'header' => Mage::helper('installation')->__('Installer'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'installer_id',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => true,
			'options' => $this->_getInstallers(),
			'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Installers_Renderer'
			));
		$this->addColumn('telephone', array(
			'header' => Mage::helper('installation')->__('Telephone'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'telephone'
			));
		$this->addColumn('last-modified', array(
			'header' => Mage::helper('installation')->__('Last Modified'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'state_change',
			'type' => 'datetime'
			));
		$this->addColumn('created-date', array(
			'header' => Mage::helper('installation')->__('Created Date'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'created_date',
			'type' => 'datetime'
			));
		$this->addColumn('action',array(
			'header' => Mage::helper('installation')->__('Action'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'id',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('installation')->__('View'),
					'url' => array('base' => '*/*/view'),
					'field' => 'id'
				)
			),
			'filter' => false,
			'sortable' => false,
			'is_system' => true
			));


		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("installation_block_adminhtml_installation_grid_preparecolumns", array("data" => $object));

		$this->addExportType('*/*/exportCsv', Mage::helper('installation')->__('CSV'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/view', array('id' => $row->getData()['id']));
	}

	/**
	 * Get Statuses
	 * ------------
	 * Build the status dropdown filter.
	 *
	 * @return	Array	The different statuses.
	 */
	private function _getStatuses()
	{
		$returnArray = [];
		$collection = Mage::getModel('installation/status')->getCollection();
		foreach($collection as $item)
		{
			if ($item->getActive() === '1') {
				$returnArray[$item->getStatusText()] = $item->getStatusText();
			}
		}
		asort($returnArray);
		return $returnArray;
	}

	/**
	 * Get Types
	 * ---------
	 * Build the advisors dropdown filter.
	 *
	 * @return	Array	The different advisors.
	 */
	private function _getAdvisors()
	{
		$return = [];
		$collection = Mage::getModel('installation/advisors')->getCollection()->addFieldToSelect(['id','name']);
		foreach ($collection->getData() as $advisor) {
			$return[$advisor['id']] = $advisor['name'];
		}
		asort($return);
		return $return;
	}

	/**
	 * Get Types
	 * ---------
	 * Build the installers dropdown filter.
	 *
	 * @return	Array	The different advisors.
	 */
	private function _getInstallers()
	{
		$return = [];
		$collection = Mage::getModel('installation/installers')->getCollection()->addFieldToSelect(['id','name']);
		foreach ($collection->getData() as $installer) {
			$return[$installer['id']] = $installer['name'];
		}
		asort($return);
		return $return;
	}
}