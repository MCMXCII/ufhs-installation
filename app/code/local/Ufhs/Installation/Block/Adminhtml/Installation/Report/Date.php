<?php

/**
* Installation Report Date
*
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
*/
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Date extends Mage_Adminhtml_Block_Widget_Form_Container
{
	protected function _prepareLayout()
	{
		return parent::_prepareLayout();
	}

	public function __construct()
	{
		parent::__construct();
		$id = $this->getRequest()->getParam('id');
		$this->_objectId = 'id';
		$this->_blockGroup = 'installation';
		$this->_controller = 'adminhtml_installation_report';
		$this->_mode = 'date';

		$this->_removeButton('save');
		$this->_removeButton('delete');
		$this->_removeButton('back');
		$this->_removeButton('reset');
	}

	public function getHeaderText()
	{
		return Mage::helper('installation')->__('Date Filter');
	}
}