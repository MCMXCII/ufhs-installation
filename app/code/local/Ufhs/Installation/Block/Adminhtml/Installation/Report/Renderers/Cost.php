<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Report_Renderers_Cost extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		$collection = Mage::getModel('installation/customer')->getCollection()
		->addFieldToFilter('state_change',array('gteq' => Mage::registry('installation-report-datefrom')))
		->addFieldToFilter('state_change',array('lteq' => Mage::registry('installation-report-dateto')))
		->addFieldToFilter('status_id',$value)
		->getColumnValues('total_cost');
		return '£' . number_format(array_sum($collection),2);
	}
}
?>