<?php
class Ufhs_Installation_Block_Adminhtml_Installation_Advisors_Renderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        return Mage::getModel('installation/advisors')->load($value)->getName();
    }
}
?>