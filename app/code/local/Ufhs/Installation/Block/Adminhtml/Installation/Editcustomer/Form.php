<?php

/**
 * Installation Data Notes
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation_Editcustomer_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');
        $customer = Mage::getModel('installation/customer')->load($id);
        $form = new Varien_Data_Form(array(
            'id' => 'editcustomer',
            'action' => $this->getUrl('*/*/updatecustomer', ['id' => $id]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('editcustomer_form', array('legend'=>Mage::helper('installation')->__('Details')));

        $fieldset->addField('first_name', 'text', [
            'label' => Mage::helper('installation')->__('First Name'),
            'name' => 'first_name',
            'value' => $customer->getFirstName() ?: '',
            'required' => true
            ]);

        $fieldset->addField('last_name', 'text', [
            'label' => Mage::helper('installation')->__('Last Name'),
            'name' => 'last_name',
            'value' => $customer->getLastName() ?: '',
            'required' => true
            ]);

        $fieldset->addField('email', 'text', [
            'label' => Mage::helper('installation')->__('Email'),
            'name' => 'email',
            'value' => $customer->getEmail() ?: '',
            'required' => true
            ]);

        $fieldset->addField('telephone', 'text', [
            'label' => Mage::helper('installation')->__('Telephone'),
            'name' => 'telephone',
            'value' => $customer->getTelephone() ?: '',
            'required' => true
            ]);

        $fieldset->addField('address1', 'text', [
            'label' => Mage::helper('installation')->__('Address Line 1'),
            'name' => 'address1',
            'value' => $customer->getAddress1() ?: '',
            'required' => true
            ]);

        $fieldset->addField('address2', 'text', [
            'label' => Mage::helper('installation')->__('Address Line 2'),
            'name' => 'address2',
            'value' => $customer->getAddress2() ?: '',
            'required' => false
            ]);

        $fieldset->addField('town', 'text', [
            'label' => Mage::helper('installation')->__('Town'),
            'name' => 'town',
            'value' => $customer->getTown() ?: '',
            'required' => true
            ]);

        $fieldset->addField('postcode', 'text', [
            'label' => Mage::helper('installation')->__('Postcode'),
            'name' => 'postcode',
            'value' => $customer->getPostcode() ?: '',
            'required' => true
            ]);

        $fieldset->addField('property', 'radios', [
            'label' => Mage::helper('installation')->__('Property Type'),
            'name' => 'property',
            'required' => true,
            'value' => $customer->getProperty() ?: 'Flat',
            'values' => [
                ['value' => 'Flat', 'label' => 'Flat'],
                ['value' => 'House', 'label' => 'House'],
                ['value' => 'Commercial', 'label' => 'Commercial']
            ]
            ]);

        $fieldset->addField('project_type', 'radios', [
            'label' => Mage::helper('installation')->__('Project Type'),
            'name' => 'project_type',
            'required' => true,
            'value' => $customer->getProjectType() ?: 'retro',
            'values' => [
                ['value' => 'retro', 'label' => 'Retro-Fit'],
                ['value' => 'newbuild', 'label' => 'New Build']
            ]
            ]);

        return parent::_prepareForm();
    }
}