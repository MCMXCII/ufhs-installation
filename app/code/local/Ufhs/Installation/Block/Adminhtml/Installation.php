<?php
/**
 * Installation Data Block
 *
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installation extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_installation';
		$this->_blockGroup = 'installation';
		$this->_headerText = Mage::helper('installation')->__('Installation Requests');
        parent::__construct();
        $this->_removeButton('add');

        $currentUser = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $roleName = Mage::getModel('admin/user')->load($currentUser)->getRole()->getData()['role_name'];

        if($roleName == 'Administrators' || $roleName == 'Install Manager') {
        $this->addButton('new_add', [
            'label' => 'Add New Job',
            'onclick' => "setLocation('" . $this->getUrl('*/*/addnew') . "')",
            'class' => 'add'
            ]);
        }

    }
}