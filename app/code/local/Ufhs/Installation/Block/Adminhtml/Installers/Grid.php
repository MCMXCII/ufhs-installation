<?php

/**
 * Installation Data Grid
 *
* @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Block_Adminhtml_Installers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('installationGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('installation/installers')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('installation')->__('ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'id'
            ));
        $this->addColumn('name', array(
            'header' => Mage::helper('installation')->__('Name'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'name'
            ));
        $this->addColumn('email', array(
            'header' => Mage::helper('installation')->__('Email'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'email'
            ));
        $this->addColumn('telephone', array(
            'header' => Mage::helper('installation')->__('Telephone'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'telephone'
            ));
        $this->addColumn('mobile', array(
            'header' => Mage::helper('installation')->__('Mobile'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'mobile'
            ));
        $this->addColumn('user_id', array(
            'header' => Mage::helper('installation')->__('Magento User'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'user_id',
            'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installers_User_Renderer'
            ));
        $this->addColumn('company', array(
            'header' => Mage::helper('installation')->__('Company'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'company'
            ));
        $this->addColumn('town', array(
            'header' => Mage::helper('installation')->__('Town'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'town'
            ));
        $this->addColumn('postcode', array(
            'header' => Mage::helper('installation')->__('Postcode'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'postcode',
            'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installation_Postcode_Renderer'
            ));
        $this->addColumn('rating', array(
            'header' => Mage::helper('installation')->__('Rating'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'id',
            'sortable' => true,
            'renderer' => 'Ufhs_Installation_Block_Adminhtml_Installers_Rating_Renderer'
            ));

        $object = new Varien_Object(array('grid_block' => $this));
        Mage::dispatchEvent("installation_block_adminhtml_installation_grid_preparecolumns", array("data" => $object));

        $this->addExportType('*/*/exportCsv', Mage::helper('installation')->__('CSV'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/installeredit', array('id' => $row->getData()['id']));
    }

    protected function _setCollectionOrder($column)
    {
        $collection = $this->getCollection();
        if ($collection) {
             switch ($column->getId()) {
                  case 'rating':
                    foreach($collection as $item)
                    {
                        $id = $item->getId();
                        $item->setRating(Mage::getModel('installation/installerrating')->getOverallRating($id));
                    }
                    $collectionItems = $collection->getItems();
                    usort($collectionItems, array($this, '_sortRating'));
                    $newCollection = new Varien_Data_Collection();
                    foreach ($collectionItems as $item) {
                        $newCollection->addItem($item);
                    }
                    $this->setCollection($newCollection);

                    break;
                default:
                    parent::_setCollectionOrder($column);
                    break;
            }
        }
        return $this;
    }

    public function _sortRating($a, $b)
    {
        $dir = $this->getParam($this->getVarNameDir(), $this->_defaultDir);
        $al = $a->getData('rating');
        $bl = $b->getData('rating');

        if ($al == $bl) {
            return 0;
        }
        if ($dir == 'asc') {
            return ($al < $bl) ? -1 : 1;
        } else {
            return ($al > $bl) ? -1 : 1;
        }
    }
}

