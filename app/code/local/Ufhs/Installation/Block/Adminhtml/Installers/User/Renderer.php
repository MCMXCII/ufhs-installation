<?php
class Ufhs_Installation_Block_Adminhtml_Installers_User_Renderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        return Mage::getModel('admin/user')->load($value)->getName();
    }
}
?>