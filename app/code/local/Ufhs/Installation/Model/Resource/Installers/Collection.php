<?php

/**
 * @version     $Id$
 * @package     Ufhs_Installation
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Installation_Model_Resource_Installers_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('installation/installers');
    }

}