<?php

/**
 * @version $Id$
 * @package Ufhs_Installation
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Installation_Model_Attachment extends Ufhs_Installation_Model_Additionsobj
{
	public function __construct()
	{
		parent::__construct('attachment');
	}
}