<?php

/**
 *
 * @version     $Id$
 * @package     Ufhs_Installation
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_Installation_Model_Advisors extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('installation/advisors');
    }

}