<?php

/**
 * @version $Id$
 * @package Ufhs_Installation
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Installation_Model_Additionsobj extends Mage_Core_Model_Abstract
{

	public function __construct($type)
	{
		$this->setType($type);
	}

	protected function _getUser()
	{
		return ucwords(Mage::getModel('admin/session')->getUser()->getUsername());
	}

	public function save()
	{
		$date = date('Y-m-d H:i:s');

		$data = [
		'type' => $this->getType(),
		'fkid' => $this->getFK(),
		'content' => $this->getContent(),
		'timestamp' => $date,
		'userstamp' => $this->_getUser()
		];
		$obj = Mage::getModel('installation/additions')->setData($data)->save();
		$this->setTempid($obj->getId());
		return $this;
	}
}