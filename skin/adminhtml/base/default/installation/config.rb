# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/adminhtml/base/default/installation/"
css_dir = "."
sass_dir = "."
images_dir = "."
generated_images_dir = "."
javascripts_dir = "."

relative_assets = true

output_style = (environment == :development) ? :expanded : :compressed
line_comments = (environment == :development) ? true : false

sass_options = {:sourcemap => true}

# compass watch -e development